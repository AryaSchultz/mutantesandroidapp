
CREATE TABLE User(
id int(4) AUTO_INCREMENT,
nome varchar(30),
senha varchar(30),
PRIMARY KEY (id)
);

CREATE TABLE Mutantes(
id int(4) AUTO_INCREMENT,
nome varchar(30),
nome_usuario varchar(30),
FOREIGN KEY (nome_usuario) REFERENCES User(id),
image BLOB
);

CREATE TABLE habilidade(
  id int(4) AUTO_INCREMENT,
  nome varchar(30),
  id_mutante int(4),
  FOREIGN KEY (id_mutante) REFERENCES Mutantes(id)

);
