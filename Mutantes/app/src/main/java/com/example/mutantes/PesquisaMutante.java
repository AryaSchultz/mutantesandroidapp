package com.example.mutantes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

public class PesquisaMutante extends AppCompatActivity {
    ListView listView ;
    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesquisa_mutante);

        String[] values = new String[] { "Apocalipse",
                "Avalanche" , "Azazel ", "Banshee ", "Basilisco ", "Bishop", "Bling",
                "Blob", "Box", "Cable", "Capitão Britânia", "Carol Danvers", "Chama Solar",
                "Chispinha", "Ciclope ", "Colossus ", "Cristal ", "Daken", "Deadpool",
                "Dentes-de-Sabre", "Emma Frost", "Espectro ", "Faísca ", "Feiticeira Escarlate",
                "Fera", "Firestar", "Franklin Richards", "Gambit", "Groxo", "Holocausto ", "Homem de Cobalto", "Illyana Rasputin", "Jean Grey", "John Sublime", "Juggernaut", "Krakoa", "Lady Letal", "Lince Negra", "Lockheed ", "Madelyne Pryor",
                "Magneto", "Malice", "Mercúrio ", "Mestre Mental", "Micromax", "Mímico" , "Míssil" , "Mística", "Nick Fury", "Ninhada" , "Noturno ", "Força Fênix",
                "Pigmeia ", "Pigmeu ", "Polaris ", "Professor Charles Xavier", "Psylocke", "Quimera ",
                "Rogue" , "Samurai de Prata", "Sasquatch ", "Sentinela ", "Siryn", "Mancha Solar", "Solaris ", "Sr. Sinistro", "Tempestade", "tropa Alfa", "Tropa Beta", "Tropa Gama", "Unus", "Wolverine",

        };
        adapter = new ArrayAdapter<String>(this,R.layout.listaxman,values);
        listView = (ListView) findViewById(R.id.list);
        EditText filtro =(EditText) findViewById(R.id.filtro);
        listView.setAdapter(adapter);

        filtro.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                (PesquisaMutante.this).adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

}
