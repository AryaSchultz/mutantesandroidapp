package com.example.mutantes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button novo_mutante;
    Button lista;
    Button sair;
    Button pequisar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void OnNovoMutante(View view){
        Intent it =new Intent(this,NovoMutante.class);
        startActivity(it);
    }

    public void OnListaMutante(View view){
        Intent it =new Intent(this,ListaMutantes.class);
        startActivity(it);
    }

    public void OnPesquisaMutante(View view){
        Intent it =new Intent(this,PesquisaMutante.class);
        startActivity(it);
    }

    public void OnSair(View view){
        finish();
        System.exit(0);
    }

}
