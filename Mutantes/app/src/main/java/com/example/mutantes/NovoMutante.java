package com.example.mutantes;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class NovoMutante extends AppCompatActivity {


    public static final int IMAGE_GALERY_REQUEST = 2154;
    private ImageView imgpic;
    byte imageInByte[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_mutante);

        imgpic=(ImageView)findViewById(R.id.imgpic);

        Button galeria =(Button)findViewById(R.id.galeria);


    }
    public void OnGaleria(View v){

        Intent pegafoto = new Intent(Intent.ACTION_PICK);

        File imagemdiretorio = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String diretorioimagemcaminho = imagemdiretorio.getPath();

        Uri data = Uri.parse(diretorioimagemcaminho);

        pegafoto.setDataAndType(data,"image/*");

        startActivityForResult(pegafoto,IMAGE_GALERY_REQUEST);

    }


    @Override
    protected void onActivityResult( int requestCode,int resultCode,Intent data){
    if (resultCode == RESULT_OK){
        // SE TUDO FOI PROCESSADO CORRETAMENTE
        if(resultCode == IMAGE_GALERY_REQUEST){
            //a galeria respondeu

            Uri imageUri = data.getData();

            InputStream inputStream;

            try {
                inputStream = getContentResolver().openInputStream(imageUri);
                Bitmap image = BitmapFactory.decodeStream(inputStream);
                imgpic.setImageBitmap(image); // setando o campo imageviwe com a imagem selecionanda usar essa aqui quando for pegar a foto para guardar

                ByteArrayOutputStream stream = new ByteArrayOutputStream(); //  pra comprimit a imagem
                image.compress(Bitmap.CompressFormat.JPEG, 100, stream); //
                imageInByte = stream.toByteArray();// pra salvar a imagem pegar essa variavel imageInByte
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this,"Não pode abrir imagem",Toast.LENGTH_LONG).show();
            }
        }
    }
    }
}
